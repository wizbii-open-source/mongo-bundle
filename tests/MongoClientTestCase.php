<?php

namespace Tests\Wizbii\OpenSource\MongoBundle;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tests\Wizbii\OpenSource\MongoBundle\Fixtures\Kernel;
use Wizbii\OpenSource\MongoBundle\MongoClientBuilderInterface;

class MongoClientTestCase extends KernelTestCase
{
    protected function setUp(): void
    {
        // ensure that Kernel class has not been previously defined by something else
        // since self::ensureKernelShutdown() does not clean this automatically
        self::$class = null;
        parent::setUp();
        $this->bootKernel();
    }

    protected static function getKernelClass(): string
    {
        if (isset($_SERVER['KERNEL_CLASS']) || isset($_ENV['KERNEL_CLASS'])) {
            if (!class_exists($class = $_ENV['KERNEL_CLASS'] ?? $_SERVER['KERNEL_CLASS'])) {
                throw new \RuntimeException(sprintf('Class "%s" doesn\'t exist or cannot be autoloaded. Check that the KERNEL_CLASS value in phpunit.xml matches the fully-qualified class name of your Kernel or override the %s::createKernel() method.', $class, static::class));
            }

            return $class;
        }

        return Kernel::class;
    }

    protected function getMongoClientBuilder(): MongoClientBuilderInterface
    {
        /** @var MongoClientBuilderInterface $mongoClientBuilder */
        $mongoClientBuilder = self::$kernel->getContainer()->get(MongoClientBuilderInterface::class);

        return $mongoClientBuilder;
    }
}
