<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\Storage\Mongo;

use MongoDB\Client;
use Psr\Log\NullLogger;
use Symfony\Component\Stopwatch\Stopwatch;
use Tests\Wizbii\OpenSource\MongoBundle\Storage\Fixtures\Dummy;
use Tests\Wizbii\OpenSource\MongoBundle\Storage\MongoClientTestSuite;
use Wizbii\JsonSerializerBundle\Serializer;
use Wizbii\OpenSource\MongoBundle\MongoClientConfiguration;
use Wizbii\OpenSource\MongoBundle\MongoClientInterface;
use Wizbii\OpenSource\MongoBundle\Storage\Mongo\MongoClient;

class MongoClientTest extends MongoClientTestSuite
{
    private MongoClient $mongoClient;
    private Client $rawClient;

    protected function setUp(): void
    {
        parent::setUp();
        $connectionString = $_SERVER['MONGODB_CONNECTION_STRING'] ?? 'mongodb://root:example@127.0.0.1:27017';
        $mongoClientConfiguration = (new MongoClientConfiguration())->setConnectionString($connectionString);
        $this->rawClient = new Client($mongoClientConfiguration->buildConnectionString());

        $this->mongoClient = new MongoClient(
            self::DATABASE,
            self::COLLECTION,
            Dummy::class,
            new NullLogger(),
            new Stopwatch(),
            new Serializer(),
            $mongoClientConfiguration,
            $this->rawClient
        );
    }

    protected function tearDown(): void
    {
        $this->rawClient->dropDatabase(self::DATABASE);
    }

    protected function getClient(): MongoClientInterface
    {
        return $this->mongoClient;
    }

    protected function isImplemented(string $method): bool
    {
        return true;
    }
}
