<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\Storage;

use MongoDB\Operation\FindOneAndUpdate;
use PHPUnit\Framework\TestCase;
use Tests\Wizbii\OpenSource\MongoBundle\Storage\Fixtures\Dummy;
use Wizbii\OpenSource\MongoBundle\Exception\DocumentDoesNotExistException;
use Wizbii\OpenSource\MongoBundle\Exception\MethodNotImplementedInThisStorageException;
use Wizbii\OpenSource\MongoBundle\MongoClientInterface;

abstract class MongoClientTestSuite extends TestCase
{
    protected const DATABASE = 'test_dummy';
    protected const COLLECTION = 'dummy';

    abstract protected function getClient(): MongoClientInterface;

    abstract protected function isImplemented(string $method): bool;

    public function testItCanPerformAggregateOperations()
    {
        $this->assertMethodIsSupported('aggregate');
        $this->loadThreeDummies();

        $different = Dummy::withId('different');
        $different->foo = 'different';

        $this->getClient()->put($different);

        $result = $this->getClient()->aggregate([
            ['$match' => ['foo' => 'bar']],
            ['$count' => 'found'],
        ]);

        $this->assertThat(iterator_to_array($result)[0]['found'], $this->equalTo(3));
    }

    public function testItCanCountDocuments()
    {
        $this->assertMethodIsSupported('count');
        $this->loadThreeDummies();

        $this->assertThat($this->getClient()->count(['_id' => ['$in' => ['first', 'second']]]), $this->equalTo(2));
    }

    public function testItCanPerformEachOperations()
    {
        $this->assertMethodIsSupported('each');
        [$first, $second, $third] = $this->loadThreeDummies();

        $dummies = $this->getClient()->each([]);

        $this->assertThat($dummies, $this->isInstanceOf(\Generator::class));
        $dummies = iterator_to_array($dummies);
        $this->assertThat($dummies, $this->countOf(3));
        $this->assertThat($dummies, $this->containsEqual($first));
        $this->assertThat($dummies, $this->containsEqual($second));
        $this->assertThat($dummies, $this->containsEqual($third));
    }

    public function testItCanFind()
    {
        $this->assertMethodIsSupported('find');
        [$first, $second, $third] = $this->loadThreeDummies();

        $dummies = $this->getClient()->find();

        $this->assertThat($dummies, $this->countOf(3));
        $this->assertThat($dummies, $this->containsEqual($first));
        $this->assertThat($dummies, $this->containsEqual($second));
        $this->assertThat($dummies, $this->containsEqual($third));
    }

    public function testItCanFindWithALimit()
    {
        $this->assertMethodIsSupported('find');
        [$first, $second, $_] = $this->loadThreeDummies();

        $dummies = $this->getClient()->find(2);

        $this->assertThat($dummies, $this->countOf(2));
        $this->assertThat($dummies, $this->containsEqual($first));
        $this->assertThat($dummies, $this->containsEqual($second));
    }

    public function testItCanFindWithAnOffset()
    {
        $this->assertMethodIsSupported('find');
        [$_, $second, $third] = $this->loadThreeDummies();

        $dummies = $this->getClient()->find(2, 1);

        $this->assertThat($dummies, $this->countOf(2));
        $this->assertThat($dummies, $this->containsEqual($second));
        $this->assertThat($dummies, $this->containsEqual($third));
    }

    public function testItCanFindWithASort()
    {
        $this->assertMethodIsSupported('find');
        [$first, $second, $third] = $this->loadThreeDummies();

        $dummies = $this->getClient()->find(3, 0, ['_id' => -1]);

        $this->assertThat($dummies, $this->countOf(3));
        $this->assertThat($dummies[0], $this->equalTo($third));
        $this->assertThat($dummies[1], $this->equalTo($second));
        $this->assertThat($dummies[2], $this->equalTo($first));
    }

    public function testItCanFindByWithAnEmptyQuery()
    {
        $this->assertMethodIsSupported('findBy');
        [$first, $second, $third] = $this->loadThreeDummies();

        $dummies = $this->getClient()->findBy([]);

        $this->assertThat($dummies, $this->countOf(3));
        $this->assertThat($dummies, $this->containsEqual($first));
        $this->assertThat($dummies, $this->containsEqual($second));
        $this->assertThat($dummies, $this->containsEqual($third));
    }

    public function testItCanFindByWithAnValidQuery()
    {
        $this->assertMethodIsSupported('findBy');
        [$_, $second, $_] = $this->loadThreeDummies();

        $dummies = $this->getClient()->findBy(['_id' => ['$in' => ['first', 'second']]], 1, 0, ['_id' => -1]);

        $this->assertThat($dummies, $this->countOf(1));
        $this->assertThat($dummies, $this->containsEqual($second));
    }

    public function testItCanFindByNoMatchingDocument()
    {
        $this->assertMethodIsSupported('findBy');
        $this->loadThreeDummies();

        $dummies = $this->getClient()->findBy(['_id' => 'fourth']);

        $this->assertThat($dummies, $this->isEmpty());
    }

    public function testItCanFindOneSingleExistingDocument()
    {
        $this->assertMethodIsSupported('findOne');
        [$first, $_, $_] = $this->loadThreeDummies();

        $dummy = $this->getClient()->findOne(['_id' => 'first']);

        $this->assertThat($dummy, $this->equalTo($first));
    }

    public function testItCanFindOneExistingDocumentAmongstMany()
    {
        $this->assertMethodIsSupported('findOne');
        [$first, $_, $_] = $this->loadThreeDummies();

        $dummy = $this->getClient()->findOne([]);

        $this->assertThat($dummy, $this->equalTo($first));
    }

    public function testItReturnsNullWhenFindOneCannotFindMatchingDocument()
    {
        $this->assertMethodIsSupported('findOne');
        $this->loadThreeDummies();

        $dummy = $this->getClient()->findOne(['_id' => 'fourth']);

        $this->assertThat($dummy, $this->isNull());
    }

    public function testItCanFindOneDocumentUpdateItAndReturnThePreviousRevision()
    {
        $this->assertMethodIsSupported('findOneAndUpdate');
        $this->loadThreeDummies();

        $dummy = $this->getClient()->findOneAndUpdate(['_id' => 'first'], ['$set' => ['foo' => 'newBar']]);

        $this->assertThat($dummy->id, $this->equalTo('first'));
        $this->assertThat($dummy->foo, $this->equalTo('bar'));
    }

    public function testItCanFindOneDocumentUpdateItAndReturnIt()
    {
        $this->assertMethodIsSupported('findOneAndUpdate');
        $this->loadThreeDummies();

        $dummy = $this->getClient()->findOneAndUpdate(['_id' => 'first'], ['$set' => ['foo' => 'newBar']], ['returnDocument' => FindOneAndUpdate::RETURN_DOCUMENT_AFTER]);

        $this->assertThat($dummy->id, $this->equalTo('first'));
        $this->assertThat($dummy->foo, $this->equalTo('newBar'));
    }

    public function testItReturnsNullWhenFindOneAndUpdateCannotFindAnything()
    {
        $this->assertMethodIsSupported('findOneAndUpdate');
        $this->loadThreeDummies();

        $dummy = $this->getClient()->findOneAndUpdate(['_id' => 'fourth'], ['$set' => ['foo' => 'newBar']]);

        $this->assertThat($dummy, $this->isNull());
    }

    public function testItGetExistingDocument()
    {
        $this->assertMethodIsSupported('get');
        [$first, $_, $_] = $this->loadThreeDummies();

        $dummy = $this->getClient()->get('first');

        $this->assertThat($dummy, $this->equalTo($first));
    }

    public function testItThrowExceptionIfDocumentIsNotFound()
    {
        $this->assertMethodIsSupported('get');
        $this->loadThreeDummies();

        $this->expectException(DocumentDoesNotExistException::class);
        $this->getClient()->get('fourth');
    }

    public function testItCanReturnCollectionName()
    {
        $this->assertMethodIsSupported('getCollectionName');

        $collectionName = $this->getClient()->getCollectionName();
        $this->assertThat($collectionName, $this->equalTo(self::COLLECTION));
    }

    public function testItCanReturnDatabaseName()
    {
        $this->assertMethodIsSupported('getDatabaseName');

        $databaseName = $this->getClient()->getDatabaseName();
        $this->assertThat($databaseName, $this->equalTo(self::DATABASE));
    }

    public function testItCanGetDistinctValues()
    {
        $this->assertMethodIsSupported('getDistinctValues');
        $this->loadThreeDummies();

        $distinctValues = $this->getClient()->getDistinctValues('_id');
        $this->assertThat($distinctValues, $this->countOf(3));
        $this->assertThat($distinctValues, $this->containsEqual('first'));
        $this->assertThat($distinctValues, $this->containsEqual('second'));
        $this->assertThat($distinctValues, $this->containsEqual('third'));
    }

    public function testItCanGetDistinctValuesWithAQuery()
    {
        $this->assertMethodIsSupported('getDistinctValues');
        $this->getClient()->put(Dummy::withId('first')->setFoo('bar'));
        $this->getClient()->put(Dummy::withId('second')->setFoo('bar2'));
        $this->getClient()->put(Dummy::withId('third')->setFoo('bar3'));

        $distinctValues = $this->getClient()->getDistinctValues('foo', ['_id' => ['$in' => ['first', 'second']]]);
        $this->assertThat($distinctValues, $this->countOf(2));
        $this->assertThat($distinctValues, $this->containsEqual('bar'));
        $this->assertThat($distinctValues, $this->containsEqual('bar2'));
    }

    public function testItHasSomeDocument()
    {
        $this->assertMethodIsSupported('has');
        $this->loadThreeDummies();

        $this->assertThat($this->getClient()->has('first'), $this->isTrue());
    }

    public function testItDoesNotHaveSomeDocument()
    {
        $this->assertMethodIsSupported('has');
        $this->loadThreeDummies();

        $this->assertThat($this->getClient()->has('fourth'), $this->isFalse());
    }

    public function it_can_replace_a_document_with_put_when_this_document_already_exists()
    {
        $this->assertMethodIsSupported('putAll');
        $this->getClient()->put($first = Dummy::withId('first'));
        $this->assertThat($this->getClient()->count(), $this->equalTo(1));
        $this->getClient()->put($first = Dummy::withId('first')->setFoo('new_value'));
        $this->assertThat($this->getClient()->count(), $this->equalTo(1));
        $this->assertThat($this->getClient()->get('first')->foo, $this->equalTo('new_value'));
    }

    public function it_can_put_multiple_documents_at_once()
    {
        $this->assertMethodIsSupported('putAll');
        $this->getClient()->putAll([
            $first = Dummy::withId('first'),
            $second = Dummy::withId('second'),
            $third = Dummy::withId('third'),
        ]);

        $this->assertThat($this->getClient()->count(), $this->equalTo(3));
    }

    public function it_can_remove_one_existing_document()
    {
        $this->assertMethodIsSupported('remove');
        $this->loadThreeDummies();
        $result = $this->getClient()->remove('first');

        $this->assertThat($result, $this->equalTo(1));
        $this->assertThat($this->getClient()->count(), $this->equalTo(2));
        $this->assertThat($this->getClient()->has('first'), $this->isFalse());
    }

    public function it_cannot_remove_non_existing_document()
    {
        $this->assertMethodIsSupported('remove');
        $this->loadThreeDummies();
        $result = $this->getClient()->remove('fourth');

        $this->assertThat($result, $this->equalTo(0));
        $this->assertThat($this->getClient()->count(), $this->equalTo(3));
    }

    public function it_can_remove_by_a_query()
    {
        $this->assertMethodIsSupported('removeBy');
        $this->loadThreeDummies();
        $result = $this->getClient()->removeBy(['_id' => ['$in' => ['first', 'second']]]);

        $this->assertThat($result, $this->equalTo(2));
        $this->assertThat($this->getClient()->count(), $this->equalTo(1));
        $this->assertThat($this->getClient()->has('first'), $this->isFalse());
        $this->assertThat($this->getClient()->has('second'), $this->isFalse());
    }

    public function it_cannot_remove_non_existing_documents_by_a_query()
    {
        $this->assertMethodIsSupported('removeBy');
        $this->loadThreeDummies();
        $result = $this->getClient()->removeBy(['_id' => ['$in' => ['fourth', 'fives']]]);

        $this->assertThat($result, $this->equalTo(0));
        $this->assertThat($this->getClient()->count(), $this->equalTo(3));
    }

    public function testItCanUpdateMultipleDocumentsAtOnceWithAQuery()
    {
        $this->assertMethodIsSupported('update');
        $this->loadThreeDummies();

        $result = $this->getClient()->update(['_id' => ['$in' => ['first', 'second']]], ['$set' => ['foo' => 'new_value']]);
        $this->assertThat($result, $this->isTrue());

        $newValues = $this->getClient()->count(['foo' => 'new_value']);
        $this->assertThat($newValues, $this->equalTo(2));
    }

    public function testItCanUpdateOneSingleDocumentsWithAQueryEvenIfMultipleDocumentsMatch()
    {
        $this->assertMethodIsSupported('update');
        $this->loadThreeDummies();

        $result = $this->getClient()->update(['_id' => ['$in' => ['first', 'second']]], ['$set' => ['foo' => 'new_value']], false);
        $this->assertThat($result, $this->isTrue());

        $newValues = $this->getClient()->count(['foo' => 'new_value']);
        $this->assertThat($newValues, $this->equalTo(1));
    }

    public function testItCanUpdateOneDocumentWithAQuery()
    {
        $this->assertMethodIsSupported('update');
        $this->loadThreeDummies();

        $result = $this->getClient()->update(['_id' => 'first'], ['$set' => ['foo' => 'new_value']]);
        $this->assertThat($result, $this->isTrue());

        $updatedDummy = $this->getClient()->get('first');
        $this->assertThat($updatedDummy->foo, $this->equalTo('new_value'));
    }

    public function testItCanUpdateOneDocument()
    {
        $this->assertMethodIsSupported('updateOne');
        $this->loadThreeDummies();

        $result = $this->getClient()->updateOne(['_id' => ['$in' => ['first', 'second']]], ['$set' => ['foo' => 'new_value']]);
        $this->assertThat($result, $this->isTrue());

        $newValues = $this->getClient()->count(['foo' => 'new_value']);
        $this->assertThat($newValues, $this->equalTo(1));
        $updatedDummy = $this->getClient()->get('first');
        $this->assertThat($updatedDummy->foo, $this->equalTo('new_value'));
    }

    public function testItCanDropCollectionIfItExists()
    {
        $this->assertMethodIsSupported('dropCollection');
        $this->loadThreeDummies();
        $result = $this->getClient()->dropCollection();
        $this->assertThat($result, $this->isTrue());
    }

    public function testItCanDropCollectionEvenIfItDoesNotExist()
    {
        $this->assertMethodIsSupported('dropCollection');
        $this->getClient()->dropCollection();
        $result = $this->getClient()->dropCollection();
        $this->assertThat($result, $this->isTrue());
    }

    /*************
     * Utilities *
     *************/

    private function loadThreeDummies(): array
    {
        $this->getClient()->put($first = Dummy::withId('first'));
        $this->getClient()->put($second = Dummy::withId('second'));
        $this->getClient()->put($third = Dummy::withId('third'));

        return [$first, $second, $third];
    }

    private function assertMethodIsSupported(string $methodName): void
    {
        if (!$this->isImplemented($methodName)) {
            $this->expectException(MethodNotImplementedInThisStorageException::class);
        }
    }
}
