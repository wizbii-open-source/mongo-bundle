<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\Storage\Local;

use Psr\Log\NullLogger;
use Symfony\Component\Stopwatch\Stopwatch;
use Tests\Wizbii\OpenSource\MongoBundle\Storage\Fixtures\Dummy;
use Tests\Wizbii\OpenSource\MongoBundle\Storage\MongoClientTestSuite;
use Wizbii\JsonSerializerBundle\Serializer;
use Wizbii\OpenSource\MongoBundle\MongoClientInterface;
use Wizbii\OpenSource\MongoBundle\Storage\Local\File\FileDataProvider;
use Wizbii\OpenSource\MongoBundle\Storage\Local\MongoClient;

class FileMongoClientTest extends MongoClientTestSuite
{
    private const NOT_IMPLEMENTED_METHODS = ['aggregate', 'aggregateAs'];

    private MongoClient $mongoClient;
    private FileDataProvider $fileDataProvider;

    protected function setUp(): void
    {
        parent::setUp();
        $this->fileDataProvider = new FileDataProvider(__DIR__.'/database', 'test.json');
        $this->mongoClient = new MongoClient(
            self::DATABASE,
            self::COLLECTION,
            Dummy::class,
            new NullLogger(),
            new Stopwatch(),
            new Serializer(),
            $this->fileDataProvider
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->fileDataProvider->dropDirectory();
    }

    protected function getClient(): MongoClientInterface
    {
        return $this->mongoClient;
    }

    protected function isImplemented(string $method): bool
    {
        return !in_array($method, self::NOT_IMPLEMENTED_METHODS);
    }
}
