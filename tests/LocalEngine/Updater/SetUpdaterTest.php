<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Updater;

class SetUpdaterTest extends UpdaterTestCase
{
    public function getUseCases()
    {
        return [
            'simple document' => [['foo' => 'bar'], ['$set' => ['foo' => 'bar2']], ['foo' => 'bar2']],
            'embedded document' => [['foo' => ['bar' => 'baz']], ['$set' => ['foo.bar' => 'baz2']], ['foo' => ['bar' => 'baz2']]],
            'multiple document' => [['foo' => 'bar', 'foo2' => 'bar2'], ['$set' => ['foo' => 'bar2', 'foo2' => 'bar3']], ['foo' => 'bar2', 'foo2' => 'bar3']],
            'create missing fields' => [['foo' => ['bar' => 'baz']], ['$set' => ['foo2.bar' => 'baz', 'foo.bar2' => 'baz2']], ['foo' => ['bar' => 'baz', 'bar2' => 'baz2'], 'foo2' => ['bar' => 'baz']]],
        ];
    }
}
