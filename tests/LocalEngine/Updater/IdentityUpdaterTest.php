<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Updater;

class IdentityUpdaterTest extends UpdaterTestCase
{
    public function getUseCases()
    {
        return [
            'never do anything :)' => [['foo' => 'bar'], [], ['foo' => 'bar']],
        ];
    }
}
