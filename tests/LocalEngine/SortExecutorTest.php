<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine;

use PHPUnit\Framework\TestCase;
use Wizbii\OpenSource\MongoBundle\LocalEngine\SortExecutor;

class SortExecutorTest extends TestCase
{
    public function testItCanDoAscendingSortOnSingleCriteria()
    {
        $documents = [
            ['foo' => 1],
            ['foo' => 3],
            ['foo' => 2],
        ];
        $sortExecutor = new SortExecutor();
        $documents = $sortExecutor->sortDocumentsOn($documents, ['foo' => 1]);
        $this->assertThat($documents[0]['foo'], $this->equalTo(1));
        $this->assertThat($documents[1]['foo'], $this->equalTo(2));
        $this->assertThat($documents[2]['foo'], $this->equalTo(3));
    }

    public function testItCanDoDescendingSortOnSingleCriteria()
    {
        $documents = [
            ['foo' => 1],
            ['foo' => 3],
            ['foo' => 2],
        ];
        $sortExecutor = new SortExecutor();
        $documents = $sortExecutor->sortDocumentsOn($documents, ['foo' => -1]);
        $this->assertThat($documents[0]['foo'], $this->equalTo(3));
        $this->assertThat($documents[1]['foo'], $this->equalTo(2));
        $this->assertThat($documents[2]['foo'], $this->equalTo(1));
    }

    public function testItCanDoSortOnMissingField()
    {
        $documents = [
            ['_id' => 'a', 'foo' => 1],
            ['_id' => 'b', 'foo' => 3],
            ['_id' => 'c', 'bar' => 2],
        ];
        $sortExecutor = new SortExecutor();
        $documents = $sortExecutor->sortDocumentsOn($documents, ['foo' => -1]);
        $this->assertThat($documents[0]['_id'], $this->equalTo('b'));
        $this->assertThat($documents[1]['_id'], $this->equalTo('a'));
        $this->assertThat($documents[2]['_id'], $this->equalTo('c'));
    }

    public function testItCanSortOnMultipleCriterion()
    {
        $documents = [
            ['_id' => 'a', 'foo' => 1, 'bar' => 1],
            ['_id' => 'b', 'foo' => 2, 'bar' => 3],
            ['_id' => 'c', 'foo' => 1, 'bar' => 2],
        ];
        $sortExecutor = new SortExecutor();
        $documents = $sortExecutor->sortDocumentsOn($documents, ['foo' => 1, 'bar' => -1]);
        $this->assertThat($documents[0]['_id'], $this->equalTo('c'));
        $this->assertThat($documents[1]['_id'], $this->equalTo('a'));
        $this->assertThat($documents[2]['_id'], $this->equalTo('b'));
    }
}
