<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class ElemMatcherFilterTest extends FilterTestCase
{
    public function getUseCases()
    {
        $document = ['results' => [82, 85, 88]];

        return [
            'valid document' => [$document, ['results' => ['$elemMatch' => ['$gt' => 80, '$lt' => 85]]], true],
            'invalid document' => [$document, ['results' => ['$elemMatch' => ['$gt' => 70, '$lt' => 80]]], false],
        ];
    }
}
