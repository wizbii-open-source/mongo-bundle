<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

use Wizbii\OpenSource\MongoBundle\LocalEngine\Exception\TypeNotSupportedException;
use Wizbii\OpenSource\MongoBundle\LocalEngine\Filter\FilterFactory;

class TypeFilterTest extends FilterTestCase
{
    public function getUseCases()
    {
        return [
            'valid double (string notation)' => [['foo' => 18.0], ['foo' => ['$type' => 'double']], true],
            'valid double (int notation)' => [['foo' => 18.0], ['foo' => ['$type' => 1]], true],
            'invalid double' => [['foo' => 'bar'], ['foo' => ['$type' => 'double']], false],

            'valid string (string notation)' => [['foo' => 'bar'], ['foo' => ['$type' => 'string']], true],
            'valid string (int notation)' => [['foo' => 'bar'], ['foo' => ['$type' => 2]], true],
            'invalid string' => [['foo' => 18], ['foo' => ['$type' => 'string']], false],

            'valid object (string notation)' => [['foo' => ['bar' => 'baz']], ['foo' => ['$type' => 'object']], true],
            'valid object (int notation)' => [['foo' => ['bar' => 'baz']], ['foo' => ['$type' => 3]], true],
            'invalid object' => [['foo' => 'not an object'], ['foo' => ['$type' => 'object']], false],

            'valid array (string notation)' => [['foo' => ['bar']], ['foo' => ['$type' => 'array']], true],
            'valid array (int notation)' => [['foo' => ['bar']], ['foo' => ['$type' => 4]], true],
            'invalid array' => [['foo' => ['bar' => 'baz']], ['foo' => ['$type' => 'array']], false],

            'valid bool (string notation)' => [['foo' => true], ['foo' => ['$type' => 'bool']], true],
            'valid bool (int notation)' => [['foo' => true], ['foo' => ['$type' => 8]], true],
            'invalid bool' => [['foo' => 'not an bool'], ['foo' => ['$type' => 'bool']], false],

            'valid null (string notation)' => [['foo' => null], ['foo' => ['$type' => 'null']], true],
            'valid null (int notation)' => [['foo' => null], ['foo' => ['$type' => 10]], true],
            'invalid null' => [['foo' => 'not null'], ['foo' => ['$type' => 'null']], false],

            'valid regexp (string notation)' => [['foo' => '/^ba/'], ['foo' => ['$type' => 'regex']], true],
            'valid regexp (int notation)' => [['foo' => '/^ba/'], ['foo' => ['$type' => 11]], true],
            'invalid regexp' => [['foo' => 'not a regexp'], ['foo' => ['$type' => 'regex']], false],

            'valid int (string notation)' => [['foo' => 3], ['foo' => ['$type' => 'int']], true],
            'valid int (int notation)' => [['foo' => 3], ['foo' => ['$type' => 16]], true],
            'invalid int' => [['foo' => 'not an integer'], ['foo' => ['$type' => 'int']], false],

            'valid timestamp (string notation)' => [['foo' => 1577382667], ['foo' => ['$type' => 'timestamp']], true],
            'valid timestamp (int notation)' => [['foo' => 1577382667], ['foo' => ['$type' => 17]], true],
            'invalid timestamp' => [['foo' => 'not a timestamp'], ['foo' => ['$type' => 'timestamp']], false],

            'valid long (string notation)' => [['foo' => 1234567890], ['foo' => ['$type' => 'long']], true],
            'valid long (int notation)' => [['foo' => 1234567890], ['foo' => ['$type' => 18]], true],
            'invalid long' => [['foo' => 'not a long'], ['foo' => ['$type' => 'long']], false],

            'valid decimal (string notation)' => [['foo' => 1.23], ['foo' => ['$type' => 'decimal']], true],
            'valid decimal (int notation)' => [['foo' => 1.23], ['foo' => ['$type' => 19]], true],
            'invalid decimal' => [['foo' => 'not a decimal'], ['foo' => ['$type' => 'decimal']], false],

            'valid minKey (string notation)' => [['foo' => PHP_INT_MIN], ['foo' => ['$type' => 'minKey']], true],
            'valid minKey (int notation)' => [['foo' => PHP_INT_MIN], ['foo' => ['$type' => -1]], true],
            'invalid minKey' => [['foo' => 'not a minKey'], ['foo' => ['$type' => 'minKey']], false],

            'valid maxKey (string notation)' => [['foo' => PHP_INT_MAX], ['foo' => ['$type' => 'maxKey']], true],
            'valid maxKey (int notation)' => [['foo' => PHP_INT_MAX], ['foo' => ['$type' => 127]], true],
            'invalid maxKey' => [['foo' => 'not a maxKey'], ['foo' => ['$type' => 'maxKey']], false],
        ];
    }

    public function testItThrowsExceptionOnNotSupportedTypes()
    {
        $this->expectException(TypeNotSupportedException::class);
        $filter = (new FilterFactory())->buildFilter(['foo' => ['$type' => 'not-supported-type']]);
        $filter->matches(['foo' => 'bar']);
    }
}
