<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class NotFilterTest extends FilterTestCase
{
    public function getUseCases()
    {
        $document = ['foo' => 'bar'];

        return [
            'valid document (with existing key)' => [$document, ['foo' => ['$not' => 'bar2']], true],
            'valid document (with missing key)' => [$document, ['foo2' => ['$not' => 'bar2']], true],
            'invalid document (with existing key)' => [$document, ['foo' => ['$not' => 'bar']], false],
        ];
    }
}
