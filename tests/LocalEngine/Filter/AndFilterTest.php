<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class AndFilterTest extends FilterTestCase
{
    public function getUseCases()
    {
        $document = ['foo' => 'bar', 'foo2' => 'bar2', 'foo3' => 'bar3'];

        return [
            'valid document' => [$document, ['foo' => 'bar', 'foo2' => 'bar2'], true],
            'unknown value' => [$document, ['foo' => 'bar', 'foo2' => 'bar3'], false], // must be bar2
            'missing key' => [$document, ['foo' => 'bar', 'foo4' => 'bar2'], false],   // must be foo2
        ];
    }
}
