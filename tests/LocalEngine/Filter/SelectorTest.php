<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

use PHPUnit\Framework\TestCase;
use Wizbii\OpenSource\MongoBundle\LocalEngine\Filter\Selector;
use Wizbii\OpenSource\MongoBundle\LocalEngine\Filter\TrueFilter;

class SelectorTest extends TestCase
{
    public function testItCanSelectASimpleScalar()
    {
        $document = ['foo' => 'bar'];
        $selector = new Selector('foo', new TrueFilter());
        $this->assertThat($selector->select('foo', $document), $this->equalTo('bar'));
    }

    public function testItCanSelectANestedScalar()
    {
        $document = ['foo1' => ['foo2' => ['foo3' => 'bar']]];
        $selector = new Selector('foo', new TrueFilter());
        $this->assertThat($selector->select('foo1.foo2.foo3', $document), $this->equalTo('bar'));
    }

    public function testItCanSelectANestedSubDocument()
    {
        $document = ['foo1' => ['foo2' => ['foo3' => 'bar']]];
        $selector = new Selector('foo', new TrueFilter());
        $this->assertThat($selector->select('foo1.foo2', $document), $this->equalTo(['foo3' => 'bar']));
    }

    public function testItDoesNotFailWhenThereIsNoMatch()
    {
        $document = ['foo' => 'bar'];
        $selector = new Selector('foo', new TrueFilter());
        $this->assertThat($selector->select('baz', $document), $this->isNull());
    }

    public function testItDoesNotFailWhenThereIsNoSubDocument()
    {
        $document = ['foo1' => ['foo2' => ['foo3' => 'bar']]];
        $selector = new Selector('foo', new TrueFilter());
        $this->assertThat($selector->select('foo1.bar', $document), $this->isNull());
    }
}
