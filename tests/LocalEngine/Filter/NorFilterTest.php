<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class NorFilterTest extends FilterTestCase
{
    public function getUseCases()
    {
        $document = ['foo' => 'bar', 'foo2' => 'bar2', 'foo3' => 'bar3'];

        return [
            'valid document' => [$document, ['$nor' => ['foo' => 'bar2', 'foo5' => 'bar4']], true],
            'unknown value' => [$document, ['$nor' => ['foo' => 'bar', 'foo2' => 'bar3']], false],
        ];
    }
}
