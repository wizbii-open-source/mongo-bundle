<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

use PHPUnit\Framework\TestCase;
use Wizbii\OpenSource\MongoBundle\LocalEngine\Filter\FilterFactory;

abstract class FilterTestCase extends TestCase
{
    /**
     * @dataProvider getUseCases
     */
    public function testFilter(array $document, array $query, bool $mustMatch, bool $debug = false)
    {
        $filter = (new FilterFactory())->buildFilter($query);
        if ($debug) {
            echo $filter->debug();
        }
        $this->assertThat($filter->matches($document), $this->equalTo($mustMatch));
    }

    abstract public function getUseCases();
}
