<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class EqualsFilterTest extends FilterTestCase
{
    public function getUseCases()
    {
        $document = ['foo' => 'bar'];

        return [
            'valid document (short syntax)' => [$document, ['foo' => 'bar'], true],
            'valid document (long syntax with $eq)' => [$document, ['foo' => ['$eq' => 'bar']], true],
            'invalid document - bad value' => [$document, ['foo' => 'bar2'], false],
            'invalid document - missing key' => [$document, ['foo2' => 'bar2'], false],
        ];
    }
}
