<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class InFilterTest extends FilterTestCase
{
    public function getUseCases()
    {
        $document = [
            'foo' => 'bar',
            'multipleValues' => ['value1', 'value2'],
        ];

        return [
            'valid document (single match)' => [$document, ['foo' => ['$in' => ['bar', 'bar2']]], true],
            'valid document (multiple match)' => [$document, ['foo' => ['$in' => ['bar', 'biz']]], true],
            'valid document (regexp)' => [$document, ['foo' => ['$in' => ['/^ba/', '/^ab/']]], true],
            'invalid document (with missing key)' => [$document, ['foo2' => ['$in' => ['bar', 'bar2']]], false],
            'invalid document (with existing key)' => [$document, ['foo' => ['$in' => ['bar2', 'bar3']]], false],
            'invalid document (with existing key - regexp)' => [$document, ['foo' => ['$in' => ['/^ab/']]], false],
            'valid document (match in array)' => [$document, ['multipleValues' => ['$in' => ['foo', 'value1', 'biz']]], true],
            'invalid document (match in array)' => [$document, ['multipleValues' => ['$in' => ['foo', 'biz']]], false],
        ];
    }
}
