<?php

namespace Tests\Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class GreaterThanOrEqualsFilterTest extends FilterTestCase
{
    public function getUseCases()
    {
        $document = ['foo' => 3];

        return [
            'valid document' => [$document, ['foo' => ['$gte' => 3]], true],
            'invalid document (with existing key)' => [$document, ['foo' => ['$gte' => 4]], false],
            'invalid document (with missing key)' => [$document, ['foo2' => ['$gte' => 2]], false],
        ];
    }
}
