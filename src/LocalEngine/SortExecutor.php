<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine;

class SortExecutor
{
    public function sortDocumentsOn(array $documents, array $sortCriteria): array
    {
        if (empty($sortCriteria) || empty($documents)) {
            return $documents;
        }
        /** @var string $firstCriteriaKey */
        $firstCriteriaKey = array_key_first($sortCriteria);
        $firstCriteriaValue = $sortCriteria[$firstCriteriaKey];
        unset($sortCriteria[$firstCriteriaKey]);
        usort($documents, function (array $document1, array $document2) use ($firstCriteriaKey, $firstCriteriaValue) {
            $document1Value = $this->getValueInsideDocument($document1, $firstCriteriaKey);
            $document2Value = $this->getValueInsideDocument($document2, $firstCriteriaKey);
            if ($firstCriteriaValue > 0) {
                return $document1Value > $document2Value ? 1 : -1;
            }

            return $document1Value > $document2Value ? -1 : 1;
        });
        if (empty($sortCriteria)) {
            return $documents;
        }

        // there are more sorts to execute. recursive calls
        $sortedDocuments = [];
        $currentPartition = [];
        $currentValue = $this->getValueInsideDocument($documents[0], $firstCriteriaKey);
        foreach ($documents as $document) {
            if ($currentValue === $this->getValueInsideDocument($document, $firstCriteriaKey)) {
                $currentPartition[] = $document;
            } else {
                $sortedDocuments = array_merge($sortedDocuments, $this->sortDocumentsOn($currentPartition, $sortCriteria));
                $currentValue = $this->getValueInsideDocument($document, $firstCriteriaKey);
                $currentPartition = [$document];
            }
        }
        $sortedDocuments = array_merge($sortedDocuments, $this->sortDocumentsOn($currentPartition, $sortCriteria));

        return $sortedDocuments;
    }

    /**
     * @return mixed|null
     */
    private function getValueInsideDocument(array $document, string $path)
    {
        $parts = explode('.', $path);
        /** @var string $firstPart */
        $firstPart = array_shift($parts);
        if (!array_key_exists($firstPart, $document)) {
            return null;
        } elseif (empty($parts)) {
            return $document[$firstPart];
        }

        return $this->getValueInsideDocument($document[$firstPart], join('.', $parts));
    }
}
