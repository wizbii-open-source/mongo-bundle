<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

interface Filter
{
    public function matches(mixed $value): bool;

    public function debug(int $nbSpaces = 0): string;
}
