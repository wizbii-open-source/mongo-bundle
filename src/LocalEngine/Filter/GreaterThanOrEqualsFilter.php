<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class GreaterThanOrEqualsFilter implements Filter
{
    private mixed $value;

    public function __construct(mixed $value)
    {
        $this->value = $value;
    }

    public function matches(mixed $value): bool
    {
        return !is_null($value) && $value >= $this->value;
    }

    /** @codeCoverageIgnore */
    public function debug(int $nbSpaces = 0): string
    {
        return str_repeat(' ', $nbSpaces)."GreaterThanOrEqualsFilter: '".$this->value."'";
    }
}
