<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class EqualsFilter implements Filter
{
    private mixed $value;

    public function __construct(mixed $value)
    {
        $this->value = $value;
    }

    public function matches(mixed $value): bool
    {
        if (is_array($value)) {
            return in_array($this->value, $value);
        }

        return $this->value === $value;
    }

    /** @codeCoverageIgnore */
    public function debug(int $nbSpaces = 0): string
    {
        return str_repeat(' ', $nbSpaces)."EqualsFilter: '".$this->value."'";
    }
}
