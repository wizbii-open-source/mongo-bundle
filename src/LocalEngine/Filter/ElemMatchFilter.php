<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class ElemMatchFilter implements Filter
{
    /** @var Filter[] */
    private array $filters;

    /** @param Filter[] $filters */
    public function __construct(array $filters)
    {
        $this->filters = $filters;
    }

    public function matches(mixed $value): bool
    {
        if (!is_array($value)) {
            return false;
        }
        foreach ($value as $v) {
            $matches = true;
            foreach ($this->filters as $filter) {
                $matches &= $filter->matches($v);
            }
            if ($matches) {
                return true;
            }
        }

        return false;
    }

    /** @codeCoverageIgnore */
    public function debug(int $nbSpaces = 0): string
    {
        $content = str_repeat(' ', $nbSpaces).'ElemMatchFilter: '."\n";
        foreach ($this->filters as $filter) {
            $content .= $filter->debug($nbSpaces + 2)."\n";
        }

        return $content;
    }
}
