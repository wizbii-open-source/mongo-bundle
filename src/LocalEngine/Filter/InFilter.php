<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class InFilter implements Filter
{
    private array $values;

    public function __construct(array $values)
    {
        $this->values = $values;
    }

    public function matches(mixed $value): bool
    {
        if (is_string($value)) {
            if ($this->doValuesMatch($value)) {
                return true;
            }
        }

        if (is_array($value)) {
            foreach ($value as $v) {
                if ($this->doValuesMatch($v)) {
                    return true;
                }
            }
        }

        return false;
    }

    /** @codeCoverageIgnore */
    public function debug(int $nbSpaces = 0): string
    {
        return str_repeat(' ', $nbSpaces)."InFilter: '".var_export($this->values, true)."'";
    }

    private function startsWith(string $haystack, string $needle): bool
    {
        // search backwards starting from haystack length characters from the end
        return $needle === '' || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }

    private function doValuesMatch(string $value): bool
    {
        foreach ($this->values as $v) {
            if ($this->startsWith($v, '/') && preg_match($v, $value) === 1) {
                return true;
            } elseif ($v === $value) {
                return true;
            }
        }

        return false;
    }
}
