<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class TrueFilter implements Filter
{
    public function matches(mixed $value): bool
    {
        return true;
    }

    /** @codeCoverageIgnore */
    public function debug(int $nbSpaces = 0): string
    {
        return str_repeat(' ', $nbSpaces).'TrueFilter';
    }
}
