<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class NotFilter implements Filter
{
    private Filter $filter;

    public function __construct(Filter $filter)
    {
        $this->filter = $filter;
    }

    public function matches(mixed $value): bool
    {
        return !$this->filter->matches($value);
    }

    /** @codeCoverageIgnore */
    public function debug(int $nbSpaces = 0): string
    {
        return str_repeat(' ', $nbSpaces)."NotFilter: \n".$this->filter->debug($nbSpaces + 2);
    }
}
