<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Filter;

class AndFilter implements Filter
{
    /** @var Filter[] */
    private array $filters;

    /** @param Filter[] $filters */
    public function __construct(array $filters)
    {
        $this->filters = $filters;
    }

    public function matches(mixed $value): bool
    {
        foreach ($this->filters as $filter) {
            if (!$filter->matches($value)) {
                return false;
            }
        }

        return true;
    }

    /** @codeCoverageIgnore */
    public function debug(int $nbSpaces = 0): string
    {
        $content = str_repeat(' ', $nbSpaces).'AndFilter: '."\n";
        foreach ($this->filters as $filter) {
            $content .= $filter->debug($nbSpaces + 2)."\n";
        }

        return $content;
    }
}
