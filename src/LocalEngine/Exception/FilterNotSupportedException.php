<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Exception;

class FilterNotSupportedException extends \Exception
{
    public function __construct(string $filter, int $code = 0, ?\Throwable $previous = null)
    {
        parent::__construct("Filter '$filter' is not supported by this library. Open a MR to support it", $code, $previous);
    }
}
