<?php

namespace Wizbii\OpenSource\MongoBundle\LocalEngine\Updater;

interface Updater
{
    public function execute(array $document): array;

    public function debug(int $nbSpaces = 0): string;
}
