<?php

namespace Wizbii\OpenSource\MongoBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/** @codeCoverageIgnore */
class WizbiiOpenSourceMongoExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../../config'));
        $loader->load('services.yaml');

        if ('test' === $container->getParameter('kernel.environment')) {
            $loader->load('services_test.yaml');
        }

        if ('functional_test' === $container->getParameter('kernel.environment')) {
            $loader->load('services_functional_test.yaml');
        }
    }

    /**
     * Allow an extension to prepend the extension configurations.
     */
    public function prepend(ContainerBuilder $container): void
    {
        /** @var string $logsDir */
        $logsDir = $container->getParameter('kernel.logs_dir');
        $container->prependExtensionConfig(
            'monolog',
            [
                'channel' => [
                    'mongo',
                ],
                'handlers' => [
                    'mongo' => [
                        'type' => 'stream',
                        'level' => 'debug',
                        'path' => $logsDir.'/mongo.log',
                        'channels' => 'mongo',
                    ],
                ],
            ]
        );
    }
}
