<?php

namespace Wizbii\OpenSource\MongoBundle\Exception;

class DocumentDoesNotExistException extends \Exception
{
    public const EXCEPTION_CODE = 1;

    public function __construct(string $databaseName, string $collectionName, string $documentId, ?\Throwable $previous = null)
    {
        parent::__construct("Document with id '$documentId' does not exist in collection '$collectionName' in database '$databaseName'", self::EXCEPTION_CODE, $previous);
    }
}
