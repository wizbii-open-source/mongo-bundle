<?php

namespace Wizbii\OpenSource\MongoBundle\Exception;

class InvalidParameterException extends \Exception
{
    public const EXCEPTION_CODE = 1;

    public function __construct(string $missingField, ?\Throwable $previous = null)
    {
        parent::__construct("Unable to build MongoClient. Missing $missingField parameter", self::EXCEPTION_CODE, $previous);
    }
}
