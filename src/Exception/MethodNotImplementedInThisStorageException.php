<?php

namespace Wizbii\OpenSource\MongoBundle\Exception;

class MethodNotImplementedInThisStorageException extends \Exception
{
    public function __construct(string $storage, string $method)
    {
        parent::__construct("Method '$method' not implemented in storage '$storage'");
    }
}
