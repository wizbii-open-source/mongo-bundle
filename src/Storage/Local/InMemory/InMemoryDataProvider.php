<?php

namespace Wizbii\OpenSource\MongoBundle\Storage\Local\InMemory;

use Wizbii\OpenSource\MongoBundle\Storage\Local\DataProvider;

class InMemoryDataProvider implements DataProvider
{
    private array $documents;

    public function __construct(array $documents = [])
    {
        $this->documents = $documents;
    }

    public function getAll(): array
    {
        return $this->documents;
    }

    public function saveAll(array $documents): void
    {
        $this->documents = $documents;
    }

    public function removeAll(): bool
    {
        $this->documents = [];

        return true;
    }
}
