<?php

namespace Wizbii\OpenSource\MongoBundle\Storage\Local\File;

use Psr\Log\NullLogger;
use Symfony\Component\Stopwatch\Stopwatch;
use Wizbii\JsonSerializerBundle\Serializer;
use Wizbii\OpenSource\MongoBundle\MongoClientConfiguration;
use Wizbii\OpenSource\MongoBundle\MongoClientInterface;
use Wizbii\OpenSource\MongoBundle\Storage\Local\LocalMongoClientBuilder;
use Wizbii\OpenSource\MongoBundle\Storage\Local\MongoClient;

class MongoClientBuilder extends LocalMongoClientBuilder
{
    private string $directory;

    public function __construct(MongoClientConfiguration $mongoClientConfiguration, string $directory)
    {
        parent::__construct($mongoClientConfiguration);
        $this->directory = $directory;
    }

    public function overrideConfigurationFor(string $database, string $collection, string $managedClassName): MongoClientConfiguration
    {
        $mongoClientConfigurationCloned = (clone $this->mongoClientConfiguration)
            ->setDatabase($database)
            ->setCollection($collection)
            ->setManagedClassName($managedClassName)
        ;
        $mongoClientBuilder = new self($mongoClientConfigurationCloned, $this->directory);
        $mongoClientConfigurationCloned->setMongoClientBuilder($mongoClientBuilder);

        return $mongoClientConfigurationCloned;
    }

    protected function doBuild(): MongoClientInterface
    {
        $database = $this->mongoClientConfiguration->getDatabase();
        $collection = $this->mongoClientConfiguration->getCollection();

        return new MongoClient(
            $database,
            $collection,
            $this->mongoClientConfiguration->getManagedClassName(),
            new NullLogger(),
            new Stopwatch(),
            new Serializer(),
            new FileDataProvider($this->directory, "$database-$collection.json")
        );
    }
}
