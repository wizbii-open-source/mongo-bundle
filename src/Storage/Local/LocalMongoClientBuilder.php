<?php

namespace Wizbii\OpenSource\MongoBundle\Storage\Local;

use Wizbii\JsonSerializerBundle\ArraySerializable;
use Wizbii\OpenSource\MongoBundle\MongoClientBuilderInterface;
use Wizbii\OpenSource\MongoBundle\MongoClientConfiguration;
use Wizbii\OpenSource\MongoBundle\MongoClientInterface;

abstract class LocalMongoClientBuilder implements MongoClientBuilderInterface
{
    /**
     * @var array<string, MongoClientInterface>
     *
     * @phpstan-var array<string, MongoClientInterface<ArraySerializable>>
     */
    private array $clients = [];

    protected MongoClientConfiguration $mongoClientConfiguration;

    public function __construct(MongoClientConfiguration $mongoClientConfiguration)
    {
        $this->mongoClientConfiguration = $mongoClientConfiguration;
    }

    /**
     * @phpstan-template T of ArraySerializable
     *
     * @phpstan-param class-string<T> $managedClassName
     *
     * @phpstan-return MongoClientInterface<T>
     */
    public function buildFor(string $database, string $collection, string $managedClassName): MongoClientInterface
    {
        return $this->overrideConfigurationFor($database, $collection, $managedClassName)->end()->build();
    }

    // @phpstan-ignore-next-line Generics not defined in MongoClientConfiguration
    public function build(): MongoClientInterface
    {
        $key = $this->mongoClientConfiguration->getDatabase().'|'.$this->mongoClientConfiguration->getCollection();
        if (!array_key_exists($key, $this->clients)) {
            $this->clients[$key] = $this->doBuild();
        }

        return $this->clients[$key];
    }

    /** @phpstan-return MongoClientInterface<ArraySerializable> */
    abstract protected function doBuild(): MongoClientInterface;
}
