<?php

namespace Wizbii\OpenSource\MongoBundle\Storage\Mongo;

use MongoDB\Client;
use MongoDB\Collection;
use MongoDB\Driver\Cursor;
use MongoDB\Driver\Exception\InvalidArgumentException;
use MongoDB\Driver\Exception\RuntimeException;
use MongoDB\Driver\Exception\UnexpectedValueException;
use MongoDB\Exception\UnsupportedException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Stopwatch\Stopwatch;
use Wizbii\JsonSerializerBundle\ArraySerializable;
use Wizbii\JsonSerializerBundle\Exception\DeserializationNotSupportedException;
use Wizbii\JsonSerializerBundle\Serializer;
use Wizbii\OpenSource\MongoBundle\Exception\InvalidParameterException;
use Wizbii\OpenSource\MongoBundle\MongoClientConfiguration;
use Wizbii\OpenSource\MongoBundle\Storage\AbstractMongoClient;

/**
 * @phpstan-template T of ArraySerializable
 *
 * @phpstan-extends AbstractMongoClient<T>
 */
class MongoClient extends AbstractMongoClient
{
    private const MAX_PUT_RETRIES = 3;
    private const MAX_COUNT_RETRIES = 3;
    private const MAX_FIND_RETRIES = 3;

    private const STOPWATCH_SELECT_COLLECTION = 'mongo.selectCollection';
    private const STOPWATCH_PUT = 'mongo.put';
    private const STOPWATCH_COUNT = 'mongo.count';
    private const STOPWATCH_FINDBY = 'mongo.findBy';
    private const STOPWATCH_FIND_ONE_AND_UPDATE = 'mongo.findOneAndUpdate';
    private const STOPWATCH_READ_CURSOR = 'mongo.readCursor';
    private const STOPWATCH_RAW_EACH = 'mongo.rawEach';
    private const STOPWATCH_REMOVE = 'mongo.remove';

    private const TEXT_EXCEPTION = 'exception';
    private const TEXT_RETRY = 'retry';
    private const TEXT_UPSERT = 'upsert';
    private const TEXT_QUERY = 'query';

    private const OPTION_LIMIT = 'limit';
    private const OPTION_SKIP = 'skip';
    private const OPTION_SORT = 'sort';
    private const COUNT = 'count';
    private const FIND_BY = 'findBy';
    private const FIND_ONE_AND_UPDATE = 'findOneAndUpdate';

    protected MongoClientConfiguration $mongoClientConfiguration;
    protected Client $client;
    private Collection $collection;

    /**
     * @throws InvalidParameterException
     */
    public function __construct(string $databaseName, string $collectionName, string $managedClassName, LoggerInterface $logger, Stopwatch $stopwatch, Serializer $serializer, MongoClientConfiguration $mongoClientConfiguration, Client $client)
    {
        parent::__construct($databaseName, $collectionName, $managedClassName, $logger, $stopwatch, $serializer);
        $this->mongoClientConfiguration = $mongoClientConfiguration;
        $this->client = $client;
        $start = $this->stopwatch->start(self::STOPWATCH_SELECT_COLLECTION)->getStartTime();
        $this->collection = $client->selectCollection(
            $this->getDatabaseName(),
            $this->getCollectionName(),
            [
                'typeMap' => [
                    'array' => 'array',
                    'document' => 'array',
                    'root' => 'array',
                ],
            ]
        );
        $this->stopwatch->stop(self::STOPWATCH_SELECT_COLLECTION);
        $this->logger->debug('['.(microtime(true) - $start).'] ['.$this->getCollectionName().'] INIT');
    }

    public function aggregate(array $pipeline, bool $outputAsArray = true): \Generator
    {
        $ds = $this->collection->aggregate($pipeline);
        foreach ($ds as $d) {
            /** @var array $d */
            yield $outputAsArray ? $d : $this->readDocument($d);
        }
    }

    public function aggregateAs(array $pipeline, string $className): \Generator
    {
        $ds = $this->collection->aggregate($pipeline);
        foreach ($ds as $d) {
            /** @var array $d */
            yield $this->readDocumentAs($d, $className);
        }
    }

    public function count(array $query = [], int $retriesCount = 0): int
    {
        $start = $this->stopwatch->start(self::STOPWATCH_COUNT)->getStartTime();
        try {
            $configuredCollection = $this->collection->withOptions($this->mongoClientConfiguration->buildOptions());
            $count = $configuredCollection->countDocuments($query);
            $this->stopwatch->stop(self::STOPWATCH_COUNT);
            $this->logQuery($query, self::COUNT, '['.(microtime(true) - $start).']');
        } catch (UnexpectedValueException|UnsupportedException|InvalidArgumentException|RuntimeException $e) {
            $this->stopwatch->stop(self::STOPWATCH_COUNT);
            $this->logQuery($query, self::COUNT, '['.(microtime(true) - $start).']');

            $this->logger->error('MongoException (count) : '.$e->getMessage().' - Collection : '.$this->mongoClientConfiguration->getCollection(), [self::TEXT_EXCEPTION => $e, self::TEXT_QUERY => $query, self::TEXT_RETRY => $retriesCount]);

            if ($retriesCount < self::MAX_COUNT_RETRIES) {
                return $this->count($query, $retriesCount + 1);
            }
            throw $e;
        }

        return $count;
    }

    /** @phpstan-return \Generator<T> */
    public function each(array $query, ?int $rows = null, ?int $offset = null, ?array $sort = null): \Generator
    {
        try {
            $configuredCollection = $this->collection->withOptions($this->mongoClientConfiguration->buildOptions());
            $options = [];
            if ($rows !== null) {
                $options[self::OPTION_LIMIT] = $rows;
            }
            if ($offset !== null) {
                $options[self::OPTION_SKIP] = $offset;
            }
            if (!empty($sort)) {
                $options[self::OPTION_SORT] = $sort;
            }
            $documents = $configuredCollection->find($query, $options);

            /** @var array $document */
            foreach ($documents as $document) {
                yield $this->readDocument($document);
            }
        } catch (UnsupportedException|RuntimeException|InvalidArgumentException $e) {
            $this->logger->error(sprintf(
                'Exception caught during %s::each() - %s',
                get_class($this),
                get_class($e)
            ), [
                self::TEXT_QUERY => $query,
                self::TEXT_EXCEPTION => $e,
            ]);
            throw $e;
        }
    }

    public function eachWithProjection(array $query, array $fields, ?int $rows = null, ?int $offset = null, ?array $sort = null): \Generator
    {
        $start = microtime(true);
        $this->stopwatch->start(self::STOPWATCH_RAW_EACH);
        $documents = null;
        try {
            $configuredCollection = $this->collection->withOptions($this->mongoClientConfiguration->buildOptions());
            $options = [
                'projection' => $fields,
            ];
            if ($rows !== null) {
                $options[self::OPTION_LIMIT] = $rows;
            }
            if ($offset !== null) {
                $options[self::OPTION_SKIP] = $offset;
            }
            if ($sort !== null) {
                $options[self::OPTION_SORT] = $sort;
            }

            $documents = $configuredCollection->find($query, $options);

            foreach ($documents as $document) {
                yield $document;
            }
            $this->stopwatch->stop('mongo.rawEach');
            $this->logQuery($query, 'each', '['.(microtime(true) - $start).']');

            return true;
        } catch (\Exception $e) {
            $this->logger->warning("Can't find documents matching query", [self::TEXT_QUERY => $query]);
            $this->logger->error($e->getMessage().' in '.$e->getFile().' at line '.$e->getLine(), [self::TEXT_EXCEPTION => $e, self::TEXT_QUERY => $query]);

            $this->stopwatch->stop('mongo.each');
            $this->logQuery($query, 'each', '['.(microtime(true) - $start).']');

            return false;
        }
    }

    /** @phpstan-return T[] */
    public function findBy(array $query = [], ?int $rows = 10, int $offset = 0, ?array $sort = null, int $retriesCount = 0): array
    {
        $this->stopwatch->start(self::STOPWATCH_FINDBY);

        $start = microtime(true);

        try {
            $configuredCollection = $this->collection->withOptions($this->mongoClientConfiguration->buildOptions());
            $options = [];
            if ($rows !== null) {
                $options[self::OPTION_LIMIT] = $rows;
            }
            if ($offset !== 0) {
                $options[self::OPTION_SKIP] = $offset;
            }
            if (!empty($sort)) {
                $options[self::OPTION_SORT] = $sort;
            }
            $documents = $configuredCollection->find($query, $options);

            $mongoDuration = microtime(true) - $start;

            $this->stopwatch->stop(self::STOPWATCH_FINDBY);
            $this->logQuery($query, self::FIND_BY, '['.(microtime(true) - $start).']', "mongoDuration : $mongoDuration");

            /** @var Cursor $documents */
            return $this->readCursor($documents, $query, $rows, $offset, $sort, $retriesCount + 1);
        } catch (UnsupportedException|InvalidArgumentException|RuntimeException $e) {
            $this->logger->error(sprintf(
                'Exception caught during %s::findBy() - %s',
                get_class($this),
                get_class($e)
            ), [
                self::TEXT_QUERY => $query,
                self::TEXT_EXCEPTION => $e,
            ]);

            $this->stopwatch->stop(self::STOPWATCH_FINDBY);
            $this->logQuery($query, self::FIND_BY, '['.(microtime(true) - $start).']');
            throw $e;
        }
    }

    /** @phpstan-return T|null */
    public function findOneAndUpdate(array $query, array $update, array $options = []): ?ArraySerializable
    {
        $this->stopwatch->start(self::STOPWATCH_FIND_ONE_AND_UPDATE);
        $start = microtime(true);

        try {
            $configuredCollection = $this->collection->withOptions($this->mongoClientConfiguration->buildOptions());
            /** @var array|null $document */
            $document = $configuredCollection->findOneAndUpdate($query, $update, $options);
        } catch (UnsupportedException|InvalidArgumentException|RuntimeException $e) {
            $this->logger->error(sprintf(
                'Exception caught during %s::findOneAndUpdate() - %s',
                get_class($this),
                get_class($e)
            ), [
                self::TEXT_QUERY => $query,
                self::TEXT_EXCEPTION => $e,
            ]);

            $this->stopwatch->stop(self::STOPWATCH_FIND_ONE_AND_UPDATE);
            $this->logQuery($query, self::FIND_ONE_AND_UPDATE, '['.(microtime(true) - $start).']');
            throw $e;
        }

        $mongoDuration = microtime(true) - $start;

        $this->stopwatch->stop(self::STOPWATCH_FIND_ONE_AND_UPDATE);
        $this->logQuery($query, self::FIND_ONE_AND_UPDATE, '['.(microtime(true) - $start).']', "mongoDuration : $mongoDuration");

        return $document ? $this->readDocument($document) : null;
    }

    public function getDistinctValues(string $key, array $query = []): array
    {
        $configuredCollection = $this->collection->withOptions($this->mongoClientConfiguration->buildOptions());

        return $configuredCollection->distinct($key, $query);
    }

    /** @phpstan-param T $document */
    public function put(ArraySerializable $document, int $retriesCount = 0): bool
    {
        $start = microtime(true);
        $this->stopwatch->start(self::STOPWATCH_PUT);
        try {
            $start2 = microtime(true);
            $asArray = $this->serializer->serializeAsArray($document);
            $serializationDuration = microtime(true) - $start2;
            if (method_exists($document, 'getId')) {
                /** @var string $documentId */
                $documentId = call_user_func([$document, 'getId']); // use this instead of $document->getId() to avoid error reporting with phpstan
                $this->logger->debug('Put '.get_class($document)." with id $documentId into ".$this->mongoClientConfiguration->getDatabase().'->'.$this->mongoClientConfiguration->getCollection());
                $asArray['_id'] = $documentId;
            }
            $configuredCollection = $this->collection->withOptions($this->mongoClientConfiguration->buildOptions());
            if (!empty($asArray['_id'] ?? null)) {
                $result = $configuredCollection->replaceOne(['_id' => $asArray['_id']], $asArray, [self::TEXT_UPSERT => true]);
            } else {
                $result = $configuredCollection->insertOne($asArray);
            }
            $this->stopwatch->stop(self::STOPWATCH_PUT);
            $this->logger->debug('['.$this->getCollectionName().'] PUT. Total Duration : '.(microtime(true) - $start)." ; serialization : $serializationDuration");

            return $result->isAcknowledged();
        } catch (UnsupportedException|InvalidArgumentException|RuntimeException $e) {
            $this->logger->error('MongoException (put) : '.$e->getMessage().' - Collection : '.$this->getCollectionName(), [self::TEXT_EXCEPTION => $e, self::TEXT_RETRY => $retriesCount]);
            $this->stopwatch->stop(self::STOPWATCH_PUT);
            if ($retriesCount < self::MAX_PUT_RETRIES) {
                return $this->put($document, $retriesCount + 1);
            }
            throw $e;
        }
    }

    public function removeBy(array $query): int
    {
        $start = $this->stopwatch->start(self::STOPWATCH_REMOVE)->getStartTime();
        try {
            $configuredCollection = $this->collection->withOptions($this->mongoClientConfiguration->buildOptions());
            $result = $configuredCollection->deleteMany($query);
            $this->stopwatch->stop(self::STOPWATCH_REMOVE);
            $this->logQuery($query, 'remove', '['.(microtime(true) - $start).']');

            /** @var int $r */
            $r = $result->isAcknowledged() ? $result->getDeletedCount() : 0;

            return $r;
        } catch (UnsupportedException|InvalidArgumentException|RuntimeException $e) {
            $this->stopwatch->stop(self::STOPWATCH_REMOVE);
            $this->logQuery($query, 'remove', '['.(microtime(true) - $start).']');
            throw $e;
        }
    }

    public function update(array $query, array $update, bool $multi = true, bool $upsert = false): bool
    {
        $configuredCollection = $this->collection->withOptions($this->mongoClientConfiguration->buildOptions());
        if ($multi) {
            $result = $configuredCollection->updateMany($query, $update, ['upsert' => $upsert]);
        } else {
            $result = $configuredCollection->updateOne($query, $update, ['upsert' => $upsert]);
        }

        return $result->isAcknowledged() && ($result->getModifiedCount() >= 1 || $result->getUpsertedCount() >= 1);
    }

    public function updateOne(array $query, array $update, bool $upsert = false): bool
    {
        $configuredCollection = $this->collection->withOptions($this->mongoClientConfiguration->buildOptions());
        $result = $configuredCollection->updateOne($query, $update, ['upsert' => $upsert]);

        return $result->isAcknowledged() && ($result->getModifiedCount() == 1 || $result->getUpsertedCount() == 1);
    }

    public function dropDatabase(): bool
    {
        $this->client->dropDatabase($this->getDatabaseName());

        return true;
    }

    public function dropCollection(): bool
    {
        $this->collection->drop();

        return true;
    }

    public function createIndex(array $keys, array $options = []): bool
    {
        $this->collection->createIndex($keys, $options);

        return true;
    }

    /*************
     * Utilities *
     *************/

    public function logQuery(array $query, string $method, ?string $prepend = null, ?string $append = null): void
    {
        $message = $prepend.(empty($prepend) ? '' : ' ').'['.$this->getCollectionName().'] '.$method.' => '.json_encode($query)." $append";
        $this->logger->debug($message);
    }

    /**
     * @return ArraySerializable[]
     *
     * @phpstan-return T[]
     *
     * @throws InvalidParameterException
     */
    private function readCursor(Cursor $cursor, array $query = [], ?int $rows = 10, int $offset = 0, ?array $sort = null, int $retriesCount = 0): array
    {
        $this->stopwatch->start(self::STOPWATCH_READ_CURSOR);
        $docs = [];
        $deserializationTime = null;
        try {
            $start2 = microtime(true);
            /** @var array $document */
            foreach ($cursor as $document) {
                $docs[] = $this->readDocument($document);
            }
            $deserializationTime = microtime(true) - $start2;
        } catch (DeserializationNotSupportedException $e) {
            $this->logger->error('MongoException : '.$e->getMessage()." - Retry $retriesCount - Collection : ".$this->mongoClientConfiguration->getCollection().' - Query was : '.var_export($query, true), [self::TEXT_EXCEPTION => $e]);
            $this->stopwatch->stop(self::STOPWATCH_READ_CURSOR);
            if ($retriesCount < self::MAX_FIND_RETRIES) {
                return $this->findBy($query, $rows, $offset, $sort, $retriesCount + 1);
            }
            throw $e;
        }
        $this->logQuery($query, 'readCursor', "[$deserializationTime]");
        $this->stopwatch->stop(self::STOPWATCH_READ_CURSOR);

        return $docs;
    }
}
