<?php

namespace Wizbii\OpenSource\MongoBundle\Storage\Mongo;

use MongoDB\Client;
use MongoDB\Driver\Exception\ConnectionException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Stopwatch\Stopwatch;
use Wizbii\JsonSerializerBundle\ArraySerializable;
use Wizbii\JsonSerializerBundle\Serializer;
use Wizbii\OpenSource\MongoBundle\Exception\InvalidParameterException;
use Wizbii\OpenSource\MongoBundle\MongoClientBuilderInterface;
use Wizbii\OpenSource\MongoBundle\MongoClientConfiguration;
use Wizbii\OpenSource\MongoBundle\MongoClientInterface;

class MongoClientBuilder implements MongoClientBuilderInterface
{
    private const STOPWATCH_INIT = 'mongo.init';
    private const MAX_INIT_RETRIES = 5;

    /**
     * @var Client[]
     */
    private static array $mongoDBClients = [];
    private MongoClientConfiguration $mongoClientConfiguration;
    private LoggerInterface $logger;
    private Stopwatch $stopwatch;
    private Serializer $serializer;

    public function __construct(MongoClientConfiguration $mongoClientConfiguration, LoggerInterface $logger, Serializer $serializer, Stopwatch $stopwatch)
    {
        $this->mongoClientConfiguration = $mongoClientConfiguration;
        $this->logger = $logger;
        $this->serializer = $serializer;
        $this->stopwatch = $stopwatch;
    }

    /**
     * @phpstan-template T of ArraySerializable
     *
     * @phpstan-param class-string<T> $managedClassName
     *
     * @phpstan-return MongoClientInterface<T>
     */
    public function buildFor(string $database, string $collection, string $managedClassName): MongoClientInterface
    {
        return $this->overrideConfigurationFor($database, $collection, $managedClassName)->end()->build();
    }

    public function overrideConfigurationFor(string $database, string $collection, string $managedClassName): MongoClientConfiguration
    {
        $mongoClientConfigurationCloned = (clone $this->mongoClientConfiguration)
            ->setDatabase($database)
            ->setCollection($collection)
            ->setManagedClassName($managedClassName)
        ;
        $mongoClientBuilder = (new MongoClientBuilder(
            $mongoClientConfigurationCloned,
            $this->logger,
            $this->serializer,
            $this->stopwatch
        )
        );
        $mongoClientConfigurationCloned->setMongoClientBuilder($mongoClientBuilder);

        return $mongoClientConfigurationCloned;
    }

    /**
     * @throws InvalidParameterException
     */
    // @phpstan-ignore-next-line Generics not defined in MongoClientConfiguration
    public function build(): MongoClientInterface
    {
        $this->mongoClientConfiguration->assertValidity();
        $connectionString = $this->mongoClientConfiguration->buildConnectionString();
        $client = self::$mongoDBClients[$connectionString] ?? null;
        if (is_null($client)) {
            $client = $this->buildClient($connectionString);
            self::$mongoDBClients[$connectionString] = $client;
        }

        return new MongoClient(
            $this->mongoClientConfiguration->getDatabase(),
            $this->mongoClientConfiguration->getCollection(),
            $this->mongoClientConfiguration->getManagedClassName(),
            $this->logger,
            $this->stopwatch,
            $this->serializer,
            $this->mongoClientConfiguration,
            $client
        );
    }

    private function buildClient(string $connectionString, int $currentRetryCounter = 0): Client
    {
        $this->stopwatch->start(self::STOPWATCH_INIT);
        $newClient = null;
        try {
            $newClient = new Client($connectionString, ['connect' => true]);
            $this->stopwatch->stop(self::STOPWATCH_INIT);

            return $newClient;
        } catch (ConnectionException $e) {
            $this->stopwatch->stop(self::STOPWATCH_INIT);
            if ($currentRetryCounter > self::MAX_INIT_RETRIES) {
                throw $e;
            }

            return $this->buildClient($connectionString, ++$currentRetryCounter);
        }
    }
}
