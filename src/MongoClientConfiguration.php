<?php

namespace Wizbii\OpenSource\MongoBundle;

use MongoDB\Driver\ReadConcern;
use MongoDB\Driver\ReadPreference;
use MongoDB\Driver\WriteConcern;
use Wizbii\JsonSerializerBundle\ArraySerializable;
use Wizbii\OpenSource\MongoBundle\Exception\InvalidParameterException;

class MongoClientConfiguration
{
    /******************************
     * Mongo client configuration *
     ******************************/
    // either connectionString or protocol/host/port must be provided
    private ?string $connectionString;
    private ?string $protocol;
    private ?string $host;
    private ?int $port;
    // optional configuration
    private ?string $replicaSet;
    /** @var string|int */
    private $writeConcern;
    private ?string $readConcernLevel;
    private ?string $readPreference;
    private ?string $readPreferenceTags;
    private ?int $socketTimeoutMS;
    private ?int $writeTimeoutMS;
    private ?bool $retryWrites;

    /*************************
     * Package configuration *
     *************************/
    private string $database;
    private string $collection;
    /** @phpstan-var class-string<ArraySerializable> */
    private string $managedClassName;

    /**************************
     * Technical dependencies *
     **************************/
    private MongoClientBuilderInterface $mongoClientBuilder;

    /**
     * @throws InvalidParameterException
     */
    public function assertValidity(): void
    {
        if (empty($this->managedClassName)) {
            throw new InvalidParameterException('managedClassName');
        }
        if (empty($this->database)) {
            throw new InvalidParameterException('database');
        }
        if (empty($this->collection)) {
            throw new InvalidParameterException('collection');
        }
    }

    public function buildConnectionString(): string
    {
        if (!empty($this->connectionString)) {
            return $this->connectionString;
        }

        $connectionString = $this->protocol.'://'.$this->host.':'.$this->port.'/';
        $isFirstParameter = true;
        if (!empty($this->replicaSet)) {
            $connectionString .= '?replicaSet='.$this->replicaSet;
            $isFirstParameter = false;
        }
        if (!empty($this->socketTimeoutMS)) {
            $connectionString .= ($isFirstParameter ? '?' : '&').'wTimeoutMS='.$this->socketTimeoutMS;
            $isFirstParameter = false;
        }
        if (!empty($this->retryWrites)) {
            $connectionString .= ($isFirstParameter ? '?' : '&').'retryWrites='.$this->retryWrites;
        }

        return $connectionString;
    }

    public function end(): MongoClientBuilderInterface
    {
        return $this->mongoClientBuilder;
    }

    public function buildOptions(): array
    {
        $options = [];
        if (!empty($this->writeConcern)) {
            $options['w'] = new WriteConcern($this->writeConcern, $this->writeTimeoutMS ?? 0);
        }
        if (!empty($this->readConcernLevel)) {
            $options['readConcernLevel'] = new ReadConcern($this->readConcernLevel);
        }
        if (!empty($this->readPreference)) {
            $tagSets = [];
            if (!empty($this->readPreferenceTags) && $this->readPreference !== 'primary') {
                foreach (explode(',', $this->readPreferenceTags) as $tag) {
                    [$key, $value] = explode('=', $tag);
                    $tagSets[] = [$key => $value];
                }
            }
            $options['readPreference'] = new ReadPreference($this->readPreference, $tagSets);
        }

        return $options;
    }

    /** @return static */
    public function setConnectionString(?string $connectionString): MongoClientConfiguration
    {
        $this->connectionString = $connectionString;

        return $this;
    }

    public function getProtocol(): ?string
    {
        return $this->protocol;
    }

    /** @return static */
    public function setProtocol(string $protocol): MongoClientConfiguration
    {
        $this->protocol = $protocol;

        return $this;
    }

    public function getHost(): ?string
    {
        return $this->host;
    }

    /** @return static */
    public function setHost(string $host): MongoClientConfiguration
    {
        $this->host = $host;

        return $this;
    }

    public function getPort(): ?int
    {
        return $this->port;
    }

    /** @return static */
    public function setPort(int $port): MongoClientConfiguration
    {
        $this->port = $port;

        return $this;
    }

    public function getReplicaSet(): ?string
    {
        return $this->replicaSet;
    }

    /** @return static */
    public function setReplicaSet(string $replicaSet): MongoClientConfiguration
    {
        $this->replicaSet = $replicaSet;

        return $this;
    }

    public function getDatabase(): string
    {
        return $this->database;
    }

    /** @return static */
    public function setDatabase(string $database): MongoClientConfiguration
    {
        $this->database = $database;

        return $this;
    }

    public function getCollection(): string
    {
        return $this->collection;
    }

    /** @return static */
    public function setCollection(string $collection): MongoClientConfiguration
    {
        $this->collection = $collection;

        return $this;
    }

    /**
     * @phpstan-return class-string<ArraySerializable>
     */
    public function getManagedClassName(): string
    {
        return $this->managedClassName;
    }

    /**
     * @phpstan-param class-string<ArraySerializable> $managedClassName
     *
     * @return static
     */
    public function setManagedClassName(string $managedClassName): MongoClientConfiguration
    {
        $this->managedClassName = $managedClassName;

        return $this;
    }

    public function getSocketTimeoutMS(): ?int
    {
        return $this->socketTimeoutMS;
    }

    /** @return static */
    public function setSocketTimeoutMS(int $socketTimeoutMS): MongoClientConfiguration
    {
        $this->socketTimeoutMS = $socketTimeoutMS;

        return $this;
    }

    /** @return string|int */
    public function getWriteConcern()
    {
        return $this->writeConcern;
    }

    /** @return static */
    public function setWriteConcern(string $writeConcern): MongoClientConfiguration
    {
        $this->writeConcern = $writeConcern;

        return $this;
    }

    public function getWriteTimeoutMS(): ?int
    {
        return $this->writeTimeoutMS;
    }

    /** @return static */
    public function setWriteTimeoutMS(int $writeTimeoutMS): MongoClientConfiguration
    {
        $this->writeTimeoutMS = $writeTimeoutMS;

        return $this;
    }

    public function getReadConcernLevel(): ?string
    {
        return $this->readConcernLevel;
    }

    /** @return static */
    public function setReadConcernLevel(string $readConcernLevel): MongoClientConfiguration
    {
        $this->readConcernLevel = $readConcernLevel;

        return $this;
    }

    public function getReadPreference(): ?string
    {
        return $this->readPreference;
    }

    /** @return static */
    public function setReadPreference(string $readPreference): MongoClientConfiguration
    {
        $this->readPreference = $readPreference;

        return $this;
    }

    public function getReadPreferenceTags(): ?string
    {
        return $this->readPreferenceTags;
    }

    /** @return static */
    public function setReadPreferenceTags(string $readPreferenceTags): MongoClientConfiguration
    {
        $this->readPreferenceTags = $readPreferenceTags;

        return $this;
    }

    public function mustRetryWrites(): bool
    {
        return $this->retryWrites ?? false;
    }

    /** @return static */
    public function setRetryWrites(bool $retryWrites): MongoClientConfiguration
    {
        $this->retryWrites = $retryWrites;

        return $this;
    }

    /**
     * @return static
     */
    public function setMongoClientBuilder(MongoClientBuilderInterface $mongoClientBuilder): MongoClientConfiguration
    {
        $this->mongoClientBuilder = $mongoClientBuilder;

        return $this;
    }
}
