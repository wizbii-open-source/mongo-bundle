<?php

namespace Wizbii\OpenSource\MongoBundle;

use Wizbii\JsonSerializerBundle\ArraySerializable;
use Wizbii\OpenSource\MongoBundle\Exception\DocumentDoesNotExistException;

/**
 * @phpstan-template T of ArraySerializable
 */
interface MongoClientInterface
{
    public function aggregate(array $pipeline, bool $outputAsArray = true): \Generator;

    /**
     * @phpstan-template Y of ArraySerializable
     *
     * @phpstan-param array $pipeline
     * @phpstan-param class-string<Y> $className
     *
     * @phpstan-return \Generator<Y>
     */
    public function aggregateAs(array $pipeline, string $className): \Generator;

    public function count(array $query = [], int $retriesCount = 0): int;

    /** @phpstan-return \Generator<T> */
    public function each(array $query, ?int $rows = null, ?int $offset = null, ?array $sort = null): \Generator;

    public function eachWithProjection(array $query, array $fields, ?int $rows = null, ?int $offset = null, ?array $sort = null): \Generator;

    /** @phpstan-return T[] */
    public function find(?int $rows = 10, int $offset = 0, ?array $sort = null): array;

    /** @phpstan-return T[] */
    public function findBy(array $query = [], ?int $rows = 10, int $offset = 0, ?array $sort = null, int $retriesCount = 0): array;

    /** @phpstan-return T|null */
    public function findOne(array $query): ?ArraySerializable;

    /** @phpstan-return T|null */
    public function findOneAndUpdate(array $query, array $update, array $options = []): ?ArraySerializable;

    /**
     * @phpstan-return T
     *
     * @throws DocumentDoesNotExistException
     */
    public function get(string $id): ArraySerializable;

    public function getCollectionName(): string;

    public function getDatabaseName(): string;

    public function getDistinctValues(string $key, array $query = []): array;

    public function has(string $id): bool;

    /** @phpstan-param T $document */
    public function put(ArraySerializable $document, int $retriesCount = 0): bool;

    /** @phpstan-param T[] $documents */
    public function putAll(array $documents): bool;

    public function remove(string $id): int;

    public function removeBy(array $query): int;

    public function update(array $query, array $update, bool $multi = true, bool $upsert = false): bool;

    public function updateOne(array $query, array $update, bool $upsert = false): bool;

    public function dropDatabase(): bool;

    public function dropCollection(): bool;

    public function createIndex(array $keys, array $options = []): bool;
}
